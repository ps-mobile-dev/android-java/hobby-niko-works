package com.niko.nikoworks.Fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.niko.nikoworks.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CompanyFormFragment extends Fragment {

    private Button submitButton;
    private Button backButton;
    private EditText nameEditText, emailEditText, phoneEditText, titleEditText, descriptionEditText;
    private String name, email, phone, title, description;
    private String mail;
    public CompanyFormFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(getString(R.string.app_name));
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.app_name));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_company_form, container, false);
        backButton = (Button) view.findViewById(R.id.backButtonCompanyForm);
        nameEditText = (EditText) view.findViewById(R.id.company_name_edittext);
        emailEditText = (EditText) view.findViewById(R.id.company_email_edittext);
        phoneEditText = (EditText) view.findViewById(R.id.company_phone_edittext);
        titleEditText = (EditText) view.findViewById(R.id.project_title_edittext);
        descriptionEditText = (EditText) view.findViewById(R.id.project_description_edittext);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        submitButton = (Button) view.findViewById(R.id.submitButtonCompanyForm);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = nameEditText.getText().toString();
                email = emailEditText.getText().toString();
                phone = phoneEditText.getText().toString();
                title = titleEditText.getText().toString();
                description = descriptionEditText.getText().toString();
                if(name.matches("") ||email.matches("") ||phone.matches("") ||title.matches("") ||description.matches("") ){
                    Toast.makeText(getActivity(),getString(R.string.no_entry),Toast.LENGTH_SHORT).show();
                }
                else{
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());
                    String field = preferences.getString(getString(R.string.pref_comp_field),null);
                    mail = "Hi Nik,\n I am  "+name+", and I would like to share the work of one of my project based on "+field+".\n\nProject Title : "+title+
                            "\n\nProject Description : \n"+description+"\n\nMy contact details are given below. You can contact me through the mail id or phone number provided below...\n\n\nE-Mail : "+
                            email+"\nPhone : "+phone;

                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT,"NEW PROJECT FROM : "+name);
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.contact_email)});
                    emailIntent.putExtra(Intent.EXTRA_TEXT,mail);
                    startActivity(emailIntent);

                }
            }
        });
        return view;
    }

}
