package com.niko.nikoworks.Fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.niko.nikoworks.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EnquiryFragment extends Fragment {

    private Button submitButton;
    private Button backButton;
    private EditText nameEditText, emailEditText, phoneEditText, enquiryEditText;
    private String name, email, phone, enquiry;
    private String mail;

    public EnquiryFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(getString(R.string.app_name));
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.app_name));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_enquiry, container, false);
        nameEditText = (EditText) view.findViewById(R.id.enquiry_name_edittext);
        emailEditText = (EditText) view.findViewById(R.id.enquiry_email_edittext);
        phoneEditText = (EditText) view.findViewById(R.id.enquiry_phone_edittext);
        enquiryEditText = (EditText) view.findViewById(R.id.enquiry_edittext);

        backButton = (Button) view.findViewById(R.id.backButtonEnquiryForm);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        submitButton = (Button) view.findViewById(R.id.submitButtonEnquiryForm);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = nameEditText.getText().toString();
                email = emailEditText.getText().toString();
                phone = phoneEditText.getText().toString();
                enquiry = enquiryEditText.getText().toString();
                if (name.matches("") || email.matches("") || phone.matches("") || enquiry.matches("")) {
                    Toast.makeText(getActivity(), getString(R.string.no_choice), Toast.LENGTH_SHORT).show();
                } else {
                    mail = "";
                    mail = mail + "Hi Nik, I'm " + name + ". I have the following enquiry related to Niko.Works : \n\n" + enquiry + "\n\nMy contact details are also given below.\n\nPhone : " + phone + "\nE-Mail : " +
                            email + "\n\nThank You Nik.";
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                    emailIntent.setData(Uri.parse("mailto:"));
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "NEW ENQUIRY FROM : " + name);
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{getString(R.string.contact_email)});
                    emailIntent.putExtra(Intent.EXTRA_TEXT, mail);
                    startActivity(emailIntent);
                }
            }
        });

        return view;
    }

}
