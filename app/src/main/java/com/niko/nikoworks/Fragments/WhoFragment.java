package com.niko.nikoworks.Fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.niko.nikoworks.R;


public class WhoFragment extends Fragment {

    private Button nextButton;
    private RadioGroup radioGroup;
    Fragment fragment;
    int id;
    int choice;
    public WhoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(getString(R.string.app_name));
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getString(R.string.app_name));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_who, container, false);
        nextButton = (Button) view.findViewById(R.id.nextButtonWho);
        radioGroup = (RadioGroup) view.findViewById(R.id.whoRadioGroup);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                id = radioGroup.getCheckedRadioButtonId();
                switch (id)
                {
                    case R.id.radioButtonIncoming:
                        choice = 1;
                        fragment = new PreferenceCompanyFragment();
                        nextFragment();
                        break;
                    case R.id.radioButtonOutgoing:
                        choice = 2;
                        fragment = new StudentFormFragment();
                        nextFragment();
                        break;
                    case R.id.radioButtonEnquiry:
                        choice = 3;
                        fragment = new EnquiryFragment();
                        nextFragment();
                        break;
                    default:
                        Toast.makeText(getActivity(),getString(R.string.no_choice),Toast.LENGTH_SHORT).show();
                }

            }
        });
        return view;
    }
    private void nextFragment()
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(getString(R.string.pref_who), choice);
        editor.apply();
        FragmentTransaction fragmentTransaction;
        fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_niko,fragment);
        fragmentTransaction.addToBackStack("Fragment");
        fragmentTransaction.commit();
    }

}
